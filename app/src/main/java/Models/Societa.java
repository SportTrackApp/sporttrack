package Models;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.Date;
import java.util.Map;

public class Societa implements Parcelable {

    //Attributes
    private String id;
    private String uid;
    private String group_id;
    private String nome;
    private String breve;
    private String nazione;
    private String citta;
    private String provincia;
    private String indirizzo;
    private String cap;
    private String email;
    private String sitoweb;
    private transient Context mContext;
    private transient ContentResolver mCr;

    public Societa(@NonNull Context context){
        mContext = context;
        mCr = context.getContentResolver();
    }

    public Societa(){

    }

    public Societa(String id){
//        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
//        Query query = db.child("media").child(id);
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if(dataSnapshot.exists()){
//                    load(dataSnapshot);
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
    }

    public Societa(Map<String, Object> snap) {
        load(snap);
    }

//    public Societa(String id, ValueEventListener listener){
//        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
//        Query query = db.child("media").child(id);
//        query.addListenerForSingleValueEvent(listener);
//    }

//    public Societa(DataSnapshot dataSnapshot){
//        load(dataSnapshot);
//    }
//
    public void load(Map<String, Object> societaDB){

        this.id = societaDB.get("id").toString();
        this.group_id = societaDB.get("group_id").toString();
        this.nome = societaDB.get("nome").toString();
        this.nazione = societaDB.get("nazione").toString();
        this.email = societaDB.get("email").toString();
          }

    private void loadCreator(){
        //creator = new User(mContext, creator_id, false);
    }

    private void loadGroup(){
        //TODO group = new Group(group_id);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBreve() {
        return breve;
    }

    public void setBreve(String breve) {
        this.breve = breve;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSitoweb() {
        return sitoweb;
    }

    public void setSitoweb(String sitoweb) {
        this.sitoweb = sitoweb;
    }

    protected Societa(Parcel in) {
        this.id = in.readString();
        this.group_id = in.readString();
        this.nome = in.readString();
        this.breve = in.readString();
        this.nazione = in.readString();
        this.citta = in.readString();
        this.provincia = in.readString();
        this.indirizzo = in.readString();
        this.cap = in.readString();
        this.email = in.readString();
        this.sitoweb = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(group_id);
        dest.writeString(nome);
        dest.writeString(breve);
        dest.writeString(nazione);
        dest.writeString(citta);
        dest.writeString(provincia);
        dest.writeString(indirizzo);
        dest.writeString(cap);
        dest.writeString(email);
        dest.writeString(sitoweb);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Societa> CREATOR = new Parcelable.Creator<Societa>() {
        @Override
        public Societa createFromParcel(Parcel in) {
            return new Societa(in);
        }

        @Override
        public Societa[] newArray(int size) {
            return new Societa[size];
        }
    };

}