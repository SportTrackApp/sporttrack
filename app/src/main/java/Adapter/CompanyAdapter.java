package Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sporttrack.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Models.Societa;

import static android.content.Context.MODE_PRIVATE;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {
    private List<Societa> data;
    private Context context;

    public CompanyAdapter(@NonNull Context context) {
        this.data = new ArrayList<>();
        this.context = context;
    }

    @Override
    public CompanyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.company_list_item, parent, false);
        CompanyAdapter.ViewHolder vh = new CompanyAdapter.ViewHolder(v);
        return vh;
    }

//    private int getNotificationCount(String groupId){
//        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("notifiche", MODE_PRIVATE);
//        int count = sharedPref.getInt("chat" + groupId, 0);
//
//        count = count + sharedPref.getInt("poll" + groupId, 0);
//
//        return count;
//    }

    @Override
    public void onBindViewHolder(final CompanyAdapter.ViewHolder holder, int position) {
        final Societa societa = data.get(position);

        holder.txtName.setText(societa.getNome());


    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public void swapData(List<Societa> data){
        //if (data.size() != this.data.size()){
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
        //}
    }

    public void addGroup(Societa data){
        this.data.add(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView txtName;
        //public ImageView imgPhoto;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.company_name);

        }

        @Override
        public void onClick(View view) {
//            int position = (int) view.getTag();
//            Intent intent = new Intent(context, GroupActivity.class);
//            long groupId = CompanyAdapter.this.getItemId(position);
//            intent.putExtra("GROUPID", groupId);
//            context.startActivity(intent);
        }
    }
}
