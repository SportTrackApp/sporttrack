package com.example.sporttrack.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sporttrack.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

public class LoginActivity extends AppCompatActivity {

    private TextView txtError;
    private EditText edtEmail;
    private EditText edtPassword;
    private Button btnAccedi;
    private Button btnIscriviti;
    private Button btnPswDimenticata;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        FirebaseFirestore db = FirebaseFirestore.getInstance();
//
//        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
//                .setPersistenceEnabled(true)
//                .build();
//        db.setFirestoreSettings(settings);

        txtError = findViewById(R.id.txtError);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnAccedi = findViewById(R.id.btnAccedi);
        btnIscriviti = findViewById(R.id.btnIscriviti);
        btnPswDimenticata = findViewById(R.id.btnPswDimenticata);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser != null){
            if(currentUser.isEmailVerified()){
                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }else{
                sendEmail();
            }

        }

        setClicker();

    }

    private void setClicker(){
        btnIscriviti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!edtEmail.getText().toString().equals("") && !edtPassword.getText().toString().equals("")) {
                    CreateUser(edtEmail.getText().toString(), edtPassword.getText().toString());
                }
            }
        });

        btnAccedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtEmail.getText().toString().equals("") && !edtPassword.getText().toString().equals("")) {
                    Login(edtEmail.getText().toString(), edtPassword.getText().toString());
                }
            }
        });

        btnPswDimenticata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtEmail.getText().toString().equals("")){
                    FirebaseAuth.getInstance().sendPasswordResetEmail(edtEmail.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
//                                        Toast.makeText(LoginActivity.this, task.getException().getMessage(),
//                                                Toast.LENGTH_LONG).show();
                                    }else
                                    {
                                        ShowError(task);
                                    }
                                }
                            });
                }
            }

        });
    }

    private void Login(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    if(mAuth.getCurrentUser().isEmailVerified()){
                        String a = mAuth.getCurrentUser().getDisplayName();
                        String aa = mAuth.getApp().getName();
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName("Prova alessio").build();

                        mAuth.getCurrentUser().updateProfile(profileUpdates);
                        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        //startActivity(intent);
                    }else{
                        sendEmail();
                    }

                }else {
                    ShowError(task);
                }
            }
        });
    }

    private void CreateUser(String email, String password)
    {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            sendEmail();
                        } else {
                            ShowError(task);
                        }

                        // ...
                    }
                });
    }

    private void sendEmail(){
        mAuth.getCurrentUser().sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Perfavore controllare la mail per la verifica.",
                                    Toast.LENGTH_LONG).show();
                        }else{
                            ShowError(task);
                        }
                    }
                });
    }

    private void ShowError(@NonNull Task task){
        if(!task.isSuccessful()) {
            String messaggio = "";
            try {
                throw task.getException();
            } catch(FirebaseAuthWeakPasswordException e) {
                //messaggio = getString(R.string.error_weak_password);
                messaggio = getString(R.string.pass_weak);
                //messaggio = "Password troppo debole.";
            } catch(FirebaseAuthInvalidCredentialsException e) {
                //messaggio = "Nome utente e/o password errati.";
                messaggio = getString(R.string.pass_error);
            } catch(FirebaseAuthUserCollisionException e) {
                //messaggio = "Impossibile creare un nuovo utente. Utente già esistente.";
                messaggio = getString(R.string.user_already_exist);
            } catch (FirebaseAuthInvalidUserException e){
                //messaggio = "Nome utente errato.";
                messaggio = getString(R.string.user_error);
            } catch(Exception e) {
                messaggio = task.getException().getLocalizedMessage();
            }
            Toast.makeText(LoginActivity.this, messaggio,
                    Toast.LENGTH_LONG).show();
        }
    }

}
