package com.example.sporttrack.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sporttrack.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.zip.Inflater;

public class MenuActivity extends AppCompatActivity {

    private Button btnSocieta;
    private Button btnRotondo2;
    private Button btnGiocatore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icona2_81);



        btnSocieta = findViewById(R.id.btnSocieta);
        String stringa = "<h1><b>S</b></h1><br>SOCIETA'";
        btnRotondo2 = findViewById(R.id.btnrotondo2);
        String stringa1 = "<h1><b>P</b></h1><br>PARTITA";
        btnGiocatore = findViewById(R.id.btnGiocatore);
        String stringa2 = "<h1><b>G</b></h1><br>GIOCATORE";
        /*btnRotondo4 = findViewById(R.id.btnrotondo4);
        String stringa3 = "<h1><b>G</b></h1><br>GIOCATORE";*/
        /*btnRotondo5 = findViewById(R.id.btnrotondo5);
        String stringa4 = "<h1><b>G</b></h1><br>GIOCATORE";*/
        /*btnRotondo6 = findViewById(R.id.btnrotondo6);
        String stringa5 = "<h1><b>G</b></h1><br>GIOCATORE";*/

        btnSocieta.setText(Html.fromHtml(stringa),
                TextView.BufferType.SPANNABLE);
        btnRotondo2.setText(Html.fromHtml(stringa1),
                TextView.BufferType.SPANNABLE);
        btnGiocatore.setText(Html.fromHtml(stringa2),
                TextView.BufferType.SPANNABLE);
       /* btnRotondo4.setText(Html.fromHtml(stringa3),
                TextView.BufferType.SPANNABLE);*/
       /* btnRotondo5.setText(Html.fromHtml(stringa4),
                TextView.BufferType.SPANNABLE);*/
       /* btnRotondo6.setText(Html.fromHtml(stringa5),
                TextView.BufferType.SPANNABLE);*/

       btnSocieta.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(MenuActivity.this, SceltaSocietaActivity.class);
               startActivity(intent);
           }
       });

       btnGiocatore.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent ciao = new Intent( MenuActivity.this, CompanyRegistrationActivity.class);
               startActivity(ciao);
           }
       });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
