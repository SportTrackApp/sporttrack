package com.example.sporttrack.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sporttrack.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import Models.Societa;

public class CreateCompanyActivity extends AppCompatActivity {
    private Spinner spNazione;
    private Button btnIscriviti;
    private EditText edtNome;
    private EditText edtBreve;
    private EditText edtCitta;
    private EditText edtProvincia;
    private EditText edtIndirizzo;
    private EditText edtCAP;
    private EditText edtemail;
    private EditText edtSitoweb;
    private FirebaseFirestore db ;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_company);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //inizilizzazione firebase
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        spNazione = findViewById(R.id.spinner_nazione);
        btnIscriviti = findViewById(R.id.btnIscriviti);
        edtNome = findViewById(R.id.edtname);
        edtBreve = findViewById(R.id.edtbreve);
        edtCitta = findViewById(R.id.edtpaese);
        edtProvincia = findViewById(R.id.edtprovincia);
        edtIndirizzo = findViewById(R.id.edt_indirizzo);
        edtCAP = findViewById(R.id.edt_CAP);
        edtemail = findViewById(R.id.edt_email);
        edtSitoweb = findViewById(R.id.edt_website);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icona2_81);


        setClicker();


//        db.collection("Societa").whereEqualTo("uid", currentUser.getUid())
//                .get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful()) {
//                            for (QueryDocumentSnapshot snap : task.getResult()) {
//                                Societa tmp = new Societa(snap.getData());
//                                String id = snap.getId();
//                                String iaad = snap.getId();
//                            }
//                        } else {
//                            String a = "";
//                            //Log.w(TAG, "Error getting documents.", task.getException());
//                        }
//                    }
//                });
//
        }

        private void setClicker(){
            // Create a new user with a first and last name

            btnIscriviti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Societa societa = new Societa();
                    societa.setId("");
                    societa.setGroup_id("");
                    societa.setUid(currentUser.getUid());
                    societa.setBreve(edtBreve.getText().toString());
                    societa.setNome(edtNome.getText().toString());
                    societa.setNazione(spNazione.getSelectedItem().toString());
                    societa.setCitta(edtCitta.getText().toString());
                    societa.setProvincia(edtProvincia.getText().toString());
                    societa.setIndirizzo(edtIndirizzo.getText().toString());
                    societa.setCap(edtCAP.getText().toString());
                    societa.setEmail(edtemail.getText().toString());
                    societa.setSitoweb(edtSitoweb.getText().toString());

// Add a new document with a generated ID
                    db.collection("Societa")
                            .add(societa)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText
                                            (getApplicationContext(), "success", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText
                                            (getApplicationContext(), "Errore", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            });
                }
            });

        }

    }






