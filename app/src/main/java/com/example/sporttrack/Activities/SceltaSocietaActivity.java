package com.example.sporttrack.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sporttrack.R;

public class SceltaSocietaActivity extends AppCompatActivity {

    private Button btnSocieta_esistente;
    private Button btnSocieta_new;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scelta_societa);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icona2_81);

        btnSocieta_esistente = findViewById(R.id.btnSocieta_Esistente);
        String stringa = "SOCIETA'<br>ESISTENTE";
        btnSocieta_new = findViewById(R.id.btnSocieta_New);
        String stringa1 = "NUOVA'<br>SOCIETA'";
        btnSocieta_esistente.setText(Html.fromHtml(stringa),
                TextView.BufferType.SPANNABLE);
        btnSocieta_new.setText(Html.fromHtml(stringa1),
                TextView.BufferType.SPANNABLE);

        btnSocieta_esistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SceltaSocietaActivity.this, CompanyActivity.class);
                startActivity(intent);
            }
        });

        btnSocieta_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ciao = new Intent( SceltaSocietaActivity.this, CreateCompanyActivity.class);
                startActivity(ciao);
            }
        });
    }
}
