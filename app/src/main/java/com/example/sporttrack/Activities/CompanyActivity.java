package com.example.sporttrack.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.sporttrack.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;

import Adapter.CompanyAdapter;
import Models.Societa;

public class CompanyActivity extends AppCompatActivity {
    private FirebaseFirestore db ;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private CompanyAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        final RecyclerView rcvList = (RecyclerView) findViewById(R.id.companylist);
        final ArrayList<Societa> societa = new ArrayList<>();

        adapter = new CompanyAdapter(this);
        rcvList.setAdapter(adapter);
        rcvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rcvList.setHasFixedSize(true);

        db.collection("Societa").whereEqualTo("uid", currentUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            adapter.swapData(Collections.<Societa>emptyList());
                            for (QueryDocumentSnapshot snap : task.getResult()) {
                                Societa tmp = new Societa(snap.getData());
                                societa.add(tmp);
                            }
                            adapter.swapData(societa);
                        } else {
                            String a = "";
                            //Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }
}
